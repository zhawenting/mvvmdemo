package com.example.user.mvvmdemo;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import javax.inject.Inject;


public class BaseApplication extends Application{
//    @Inject
//    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
    private static Context appContext;
    public static Context getAppContext(){
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext =getApplicationContext();

    }


//    @Override
//    public AndroidInjector<Activity> activityInjector() {
//        return null;
//    }
}
