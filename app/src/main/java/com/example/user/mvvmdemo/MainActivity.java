package com.example.user.mvvmdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.user.mvvmdemo.d_state_lcee.features.user.UserActivity;
import com.example.user.mvvmdemo.demo1.activity.Demo1UserActivity;
import com.example.user.mvvmdemo.demo2.Demo2RepositoryActivity;
import com.example.user.mvvmdemo.demo3.features.users.Demo3RoomActivity;
import com.example.user.mvvmdemo.demo4.features.user.Demo4LCEEActivity;
import com.example.user.mvvmdemo.demo5.features.user.Demo5SimpleDataActivity;
import com.example.user.mvvmdemo.demo6.features.projects.Demo6ReadMoreActivity;
import com.example.user.mvvmdemo.demo9.DaggerDemoActivity;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void demo1(View view) {
        startActivity(new Intent(MainActivity.this, Demo1UserActivity.class));
    }

    public void demo2repository(View view) {
        startActivity(new Intent(MainActivity.this, Demo2RepositoryActivity.class));
    }

    public void demo3room(View view) {
        startActivity(new Intent(MainActivity.this, Demo3RoomActivity.class));
    }

    public void demo4room(View view) {
        startActivity(new Intent(MainActivity.this, Demo4LCEEActivity.class));
    }

    public void demo5simpledata(View view) {
        startActivity(new Intent(MainActivity.this, Demo5SimpleDataActivity.class));
    }


    public void demo9Dagger(View view) {
        startActivity(new Intent(MainActivity.this, DaggerDemoActivity.class));
    }
}
