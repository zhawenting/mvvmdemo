package com.example.user.mvvmdemo.demo1.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.user.mvvmdemo.R;
import com.example.user.mvvmdemo.demo1.model.User;
import com.example.user.mvvmdemo.demo1.viewmodel.UserViewModel;

public class Demo1UserActivity extends AppCompatActivity {


    private UserViewModel userViewModel;
    private TextView tvId;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo1_user);

        initView();
        initData();
    }

    private void initView() {
        tvId = (TextView) findViewById(R.id.tv_id);
        tvName = (TextView) findViewById(R.id.tv_name);
    }

    private void initData() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                updateView(user);
            }


        });
        userViewModel.setUser("Alex");
    }

    private void updateView(User user) {
        tvId.setText(user.getId());
        tvName.setText(user.getName());
    }
}
