package com.example.user.mvvmdemo.demo1.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.user.mvvmdemo.demo1.model.User;

public class UserViewModel extends ViewModel {


    private MutableLiveData<User> user;

    public LiveData<User> getUser(){
        if(user==null)
            user = new MutableLiveData<>();
        return user;
    }

    public void setUser(String userName){
        user.setValue(new User("1",userName));
    }

}
