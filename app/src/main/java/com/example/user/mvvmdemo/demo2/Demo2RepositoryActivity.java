package com.example.user.mvvmdemo.demo2;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.user.mvvmdemo.R;
import com.example.user.mvvmdemo.demo2.common.bean.User;
import com.example.user.mvvmdemo.demo2.features.user.UserViewModel;

public class Demo2RepositoryActivity extends AppCompatActivity {
    private static final String TAG = Demo2RepositoryActivity.class.getName();
    private UserViewModel userViewModel;
    private TextView tvId;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo2_repository);

        initView();
        initData();
    }

    private void initData() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getUser("Ben").observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                updateView(user);
            }
        });
    }

    private void updateView(User user) {
        tvId.setText(user.getId()+"");
        tvName.setText(user.getName());
    }

    private void initView() {
        tvId = (TextView) findViewById(R.id.tv_id);
        tvName = (TextView) findViewById(R.id.tv_name);
    }




}
