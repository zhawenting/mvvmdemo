package com.example.user.mvvmdemo.demo2.common.repository;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {
    private static OkHttpClient okHttpClient;
    private static Retrofit retrofit;
    public static Retrofit getInstance(){
        return retrofit;
    }

    private static final String HOST = "https://api.github.com";

    static {
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(9, TimeUnit.SECONDS).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


}
