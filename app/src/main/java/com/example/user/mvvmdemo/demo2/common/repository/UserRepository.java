package com.example.user.mvvmdemo.demo2.common.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;


import com.example.user.mvvmdemo.demo2.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.UserDateSource;
import com.example.user.mvvmdemo.demo3.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo3.common.repository.remote.RemoteUserDateSource;
import com.example.user.mvvmdemo.demo3.common.utils.NetworkUtils;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.example.user.mvvmdemo.demo2.common.bean.User;
import com.example.user.mvvmdemo.demo2.common.repository.RetrofitFactory;
import com.example.user.mvvmdemo.demo2.common.repository.UserApi;
import com.example.user.mvvmdemo.demo3.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo3.common.repository.remote.RemoteUserDateSource;
import com.example.user.mvvmdemo.demo3.common.utils.NetworkUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {

    private static final UserRepository instance = new UserRepository();

    private UserRepository(){

    }

    public static UserRepository getInstance(){
        return instance;
    }

    public UserApi userApi = RetrofitFactory.getInstance().create(UserApi.class);

    public LiveData<User> getUser(String username){
        final MutableLiveData<User> user = new MutableLiveData<>();
        userApi.queryUserByUserName(username)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        user.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        return user;
    }
}
