package com.example.user.mvvmdemo.demo3.common.repository;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo3.common.bean.User;

public interface UserDateSource {
    LiveData<User> queryUserByUsername(String username);
}
