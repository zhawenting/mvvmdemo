package com.example.user.mvvmdemo.demo3.common.repository;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.example.user.mvvmdemo.demo3.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.UserDateSource;
import com.example.user.mvvmdemo.demo3.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo3.common.repository.remote.RemoteUserDateSource;
import com.example.user.mvvmdemo.demo3.common.utils.NetworkUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {

    private static final UserRepository instance = new UserRepository();
    private Context context;

    private UserRepository() {

    }

    private UserDateSource remoteUserDateSource = RemoteUserDateSource.getInstance();
    private LocalUserDataSource localUserDateSource = LocalUserDataSource.getInstance();

    public static UserRepository getInstance() {
        return instance;
    }

    public void init(Context context) {
        this.context = context;
    }

    public LiveData<User> getUser(String username) {
        if (NetworkUtils.isConnected(context))
            return remoteUserDateSource.queryUserByUsername(username);
        else
            return localUserDateSource.queryUserByUsername(username);
    }
}
