package com.example.user.mvvmdemo.demo3.common.repository.local;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo3.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.UserDateSource;
import com.example.user.mvvmdemo.demo3.common.repository.local.service.UserService;
import com.example.user.mvvmdemo.demo3.common.repository.local.service.UserServiceImpl;

public class LocalUserDataSource implements UserDateSource{

    private static final LocalUserDataSource instance = new LocalUserDataSource();
    private LocalUserDataSource(){}
    public static LocalUserDataSource getInstance(){return instance;}

    private UserService userService = UserServiceImpl.getInstance();


    @Override
    public LiveData<User> queryUserByUsername(String username) {
        return userService.queryByUsername(username);
    }

    public LiveData<Long> addUser(User user) {
        return userService.add(user);
    }


}
