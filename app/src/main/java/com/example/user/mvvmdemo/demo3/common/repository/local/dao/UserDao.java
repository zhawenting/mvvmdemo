package com.example.user.mvvmdemo.demo3.common.repository.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.user.mvvmdemo.demo3.common.bean.User;

@Dao
public interface UserDao {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    long add(User user);

    @Query("select * from user where login =:username")
    LiveData<User> queryByUsername(String username);
}
