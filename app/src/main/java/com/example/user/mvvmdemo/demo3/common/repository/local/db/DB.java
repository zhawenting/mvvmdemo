package com.example.user.mvvmdemo.demo3.common.repository.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.user.mvvmdemo.demo3.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.local.dao.UserDao;

@Database(entities =  {User.class},version=1,exportSchema = false)
public abstract class DB extends RoomDatabase{

    public abstract UserDao getUserDao();
}
