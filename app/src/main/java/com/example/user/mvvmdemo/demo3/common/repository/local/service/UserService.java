package com.example.user.mvvmdemo.demo3.common.repository.local.service;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo3.common.bean.User;

public interface UserService {
    LiveData<Long> add(User user);
    LiveData<User> queryByUsername(String username);
}
