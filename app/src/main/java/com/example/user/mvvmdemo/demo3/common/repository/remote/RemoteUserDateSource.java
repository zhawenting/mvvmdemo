package com.example.user.mvvmdemo.demo3.common.repository.remote;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.user.mvvmdemo.demo3.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.UserDateSource;
import com.example.user.mvvmdemo.demo3.common.repository.local.LocalUserDataSource;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteUserDateSource implements UserDateSource{
    private static final RemoteUserDateSource instance = new RemoteUserDateSource();
    private RemoteUserDateSource(){}
    public static RemoteUserDateSource getInstance(){
        return instance;
    }

    private UserApi userApi = RetrofitFactory.getInstance().create(UserApi.class);


    @Override
    public LiveData<User> queryUserByUsername(final String username) {
        final MutableLiveData<User> data = new MutableLiveData<>();
        userApi.queryUserByUserName(username)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        User user = response.body();
                        if(null==user)
                            return;
                        data.setValue(user);
                        LocalUserDataSource.getInstance().addUser(user);
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });
        return data;
    }
}
