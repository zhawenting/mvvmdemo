package com.example.user.mvvmdemo.demo3.features.users;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.user.mvvmdemo.R;
import com.example.user.mvvmdemo.demo3.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.UserRepository;
import com.example.user.mvvmdemo.demo3.common.repository.local.db.DBHelper;

public class Demo3RoomActivity extends AppCompatActivity {

    private TextView tvId;
    private TextView tvName;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo3_room);
        initView();
        initData();
    }

    private void initData() {
        DBHelper.getInstance().init(this);
        UserRepository.getInstance().init(this);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getUser("Carol").observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                updateView(user);
            }
        });

    }

    private void updateView(User user) {
        tvId.setText(user.getId() + "");
        tvName.setText(user.getName());
    }


    private void initView() {
        tvId = (TextView) findViewById(R.id.tv_id);
        tvName = (TextView) findViewById(R.id.tv_name);
    }
}
