package com.example.user.mvvmdemo.demo3.features.users;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.user.mvvmdemo.demo3.common.bean.User;
import com.example.user.mvvmdemo.demo3.common.repository.UserRepository;

public class UserViewModel extends ViewModel {
    private UserRepository userRepository = UserRepository.getInstance();
    private LiveData<User> user;

    public LiveData<User> getUser(String username){
        if(null==user)
            user = userRepository.getUser(username);
        return user;
    }

}
