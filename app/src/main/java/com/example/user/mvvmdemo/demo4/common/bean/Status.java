package com.example.user.mvvmdemo.demo4.common.bean;

public enum Status {

    Loading,
    Content,
    Empty,
    Error,
}
