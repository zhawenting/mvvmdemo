package com.example.user.mvvmdemo.demo4.common.repository;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo4.common.bean.User;
import com.example.user.mvvmdemo.demo4.common.bean.Lcee;

public interface UserDataSource {
    LiveData<Lcee<User>> queryUserByUserName(String username);
}
