package com.example.user.mvvmdemo.demo4.common.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;


import com.example.user.mvvmdemo.demo4.common.repository.remote.RemoteUserDataSource;
import com.example.user.mvvmdemo.demo4.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo4.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo4.common.utils.NetworkUtils;
import com.example.user.mvvmdemo.demo4.common.bean.Lcee;
import com.example.user.mvvmdemo.demo4.common.bean.User;

public class UserRepository {

    private static final UserRepository instance = new UserRepository();
    private LocalUserDataSource localUserDataSource = LocalUserDataSource.getInstance();
    private RemoteUserDataSource remoteUserDataSource= RemoteUserDataSource.getInstance();
    private UserRepository(){}
    public static UserRepository getInstance(){
        return instance;
    }

    private Context context;

    public void init(Context context){
        this.context = context.getApplicationContext();
    }

    public LiveData<Lcee<User>> getUser(String username){
        if (NetworkUtils.isConnected(context)) {
            return remoteUserDataSource.queryUserByUserName(username);
        } else {
            return localUserDataSource.queryUserByUserName(username);
        }

    }
}
