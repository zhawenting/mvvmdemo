package com.example.user.mvvmdemo.demo4.common.repository.remote;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.example.user.mvvmdemo.demo4.common.bean.User;
import com.example.user.mvvmdemo.demo4.common.bean.Lcee;
import com.example.user.mvvmdemo.demo4.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo4.common.repository.local.LocalUserDataSource;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 86839 on 2017/10/6.
 */

public class RemoteUserDataSource implements UserDataSource {
    private static final RemoteUserDataSource instance = new RemoteUserDataSource();
    private String TAG ="RemoteUserDataSource";

    private RemoteUserDataSource() {
    }

    public static RemoteUserDataSource getInstance() {
        return instance;
    }


    private UserApi userApi = RetrofitFactory.getInstance().create(UserApi.class);

    @Override
    public LiveData<Lcee<User>> queryUserByUserName(String username) {
        final MutableLiveData<Lcee<User>> data = new MutableLiveData<>();
        data.setValue(Lcee.<User>loading());
        Log.e(TAG, "queryUserByUserName: " );

        userApi.queryUserByUsername(username)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        User user = response.body();
                        if(null==user){
                            Log.e(TAG, "user==null " );
                            data.setValue(Lcee.<User>empty());
                            return;
                        }
                        Log.e(TAG, "user!=null " );
                        data.setValue(Lcee.content(user));
                        LocalUserDataSource.getInstance().addUser(user);
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.e(TAG, "onFailure "+t.getMessage());
                        data.setValue(Lcee.<User>error(t));
                    }
                });
        return data;
    }
}
