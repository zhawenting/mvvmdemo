package com.example.user.mvvmdemo.demo4.features.user;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.mvvmdemo.R;
import com.example.user.mvvmdemo.demo4.common.bean.Lcee;
import com.example.user.mvvmdemo.demo4.common.bean.User;
import com.example.user.mvvmdemo.demo4.common.repository.UserRepository;
import com.example.user.mvvmdemo.demo4.common.repository.local.db.DBHelper;

import java.security.Key;

public class Demo4LCEEActivity extends AppCompatActivity {

    // view
    private View vContent;
    private View vError;
    private View vLoading;
    private View vEmpty;

    private TextView tvId;
    private TextView tvName;
    private EditText etUsername;
    private UserViewModel userViewModel;

    public String TAG = "Demo4LCEEActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo4_lcee);
        initView();
        initData();
        initEvent();
    }



    private void initData() {
        DBHelper.getInstance().init(this);
        UserRepository.getInstance().init(this);

         userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
         userViewModel.getIdUser().observe(this, new Observer<Lcee<User>>() {
             @Override
             public void onChanged(@Nullable Lcee<User> userLcee) {
                 Log.e(TAG, "onChanged: " );
                 updateView(userLcee);
             }
         });

         reload();

    }



    public void updateView(Lcee<User> lcee){
        switch (lcee.status){
            case Content: {
                showContent();
                Log.e(TAG, "showContent: " );
                tvId.setText(lcee.data.getId() + "");
                tvName.setText(lcee.data.getName());

                break;
            }

            case Empty:

                Log.e(TAG, "showEmpty: " );
                showEmpty();
                break;
            case Error:
                showError();
                Log.e(TAG, "showError: " );
                break;

            case Loading:
                showLoading();
                Log.e(TAG, "showLoading: " );
                break;

        }

    }


    private void reload() {
        userViewModel.reload(getUsername());
    }

    private String getUsername() {
        return etUsername.getText().toString();
    }


    private void initEvent() {
        final View.OnClickListener reloadClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                reload();
            }
        };
        vError.setOnClickListener(reloadClickListener);
        vEmpty.setOnClickListener(reloadClickListener);
        findViewById(R.id.btn_search).setOnClickListener(reloadClickListener);

        etUsername.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode== KeyEvent.KEYCODE_ENTER){
                    hideKeyboard();
                    reload();
                    return true;
                }
                return false;
            }
        });
    }

    private void hideKeyboard() {
        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(Demo4LCEEActivity.this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void showContent() {
        vContent.setVisibility(View.VISIBLE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
        vLoading.setVisibility(View.GONE);
    }

    private void showEmpty() {
        vContent.setVisibility(View.GONE);
        vEmpty.setVisibility(View.VISIBLE);
        vError.setVisibility(View.GONE);
        vLoading.setVisibility(View.GONE);
    }

    private void showError() {
        vContent.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.VISIBLE);
        vLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        vContent.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
        vLoading.setVisibility(View.VISIBLE);
    }



    private void initView() {
        vContent = findViewById(R.id.v_content);
        vError = findViewById(R.id.v_error);
        vLoading = findViewById(R.id.v_loading);
        vEmpty = findViewById(R.id.v_empty);

        tvId = (TextView) findViewById(R.id.tv_id);
        tvName = (TextView) findViewById(R.id.tv_name);
        etUsername = (EditText) findViewById(R.id.et_username);
    }


}
