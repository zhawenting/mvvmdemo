package com.example.user.mvvmdemo.demo4.features.user;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.example.user.mvvmdemo.demo4.common.bean.Lcee;
import com.example.user.mvvmdemo.demo4.common.bean.User;
import com.example.user.mvvmdemo.demo4.common.repository.UserRepository;

public class UserViewModel extends ViewModel {
    private UserRepository userRepository = UserRepository.getInstance();
    private MutableLiveData<String> idUserName;
    private LiveData<Lcee<User>> idUser;

    public LiveData<Lcee<User>> getIdUser() {
        if(null==idUser){

            idUserName = new MutableLiveData<>();
            idUser = Transformations.switchMap(idUserName, new Function<String, LiveData<Lcee<User>>>() {
                @Override
                public LiveData<Lcee<User>> apply(String username) {
                    return userRepository.getUser(username);
                }
            });
        }

        return idUser;
    }

    public void  reload(String username){
        idUserName.setValue(username);
    }
}
