package com.example.user.mvvmdemo.demo5.common.bean;

public enum Status {

    Loading,
    Content,
    Empty,
    Error,
}
