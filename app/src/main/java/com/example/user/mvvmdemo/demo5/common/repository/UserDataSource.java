package com.example.user.mvvmdemo.demo5.common.repository;

import com.example.user.mvvmdemo.demo5.common.bean.User;

public interface UserDataSource {
    interface Result<T> {
        void onSuccess(T data);
        void onFailed(Throwable throwable);
    }

    void queryUserByUserName(String username, Result<User> result);
}
