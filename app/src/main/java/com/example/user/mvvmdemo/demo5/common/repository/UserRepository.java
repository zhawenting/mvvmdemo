package com.example.user.mvvmdemo.demo5.common.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.example.user.mvvmdemo.demo5.common.bean.Lcee;
import com.example.user.mvvmdemo.demo5.common.bean.User;
import com.example.user.mvvmdemo.demo5.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo5.common.repository.remote.RemoteUserDataSource;
import com.example.user.mvvmdemo.demo5.common.utils.NetworkUtils;

import javax.sql.DataSource;

public class UserRepository {

    private static final UserRepository instance = new UserRepository();
    private LocalUserDataSource localUserDataSource = LocalUserDataSource.getInstance();
    private RemoteUserDataSource remoteUserDataSource= RemoteUserDataSource.getInstance();
    private UserRepository(){}
    public static UserRepository getInstance(){
        return instance;
    }

    private Context context;

    public void init(Context context){
        this.context = context.getApplicationContext();
    }

    public LiveData<Lcee<User>> getUser(String username){
        if (NetworkUtils.isConnected(context)) {
            return getUserFromRemote(username);
        } else {
            return getUserFromLocal(username);
        }

    }

    private LiveData<Lcee<User>> getUserFromLocal(String username) {
        return getUserFromDataSource(localUserDataSource,username);

    }

    private LiveData<Lcee<User>> getUserFromRemote(String username) {

        return getUserFromDataSource(remoteUserDataSource,username);
    }

    private LiveData<Lcee<User>> getUserFromDataSource(UserDataSource userDataSource, String username) {
        final MutableLiveData<Lcee<User>> data = new MutableLiveData<>();
        data.setValue(Lcee.<User>loading());
        userDataSource.queryUserByUserName(username, new UserDataSource.Result<User>() {
            @Override
            public void onSuccess(User user) {
                if(null==data)
                    data.setValue(Lcee.<User>empty());
                else
                    data.setValue(Lcee.content(user));
            }

            @Override
            public void onFailed(Throwable throwable) {
                data.setValue(Lcee.<User>error(throwable));
            }
        });
        return data;
    }



}
