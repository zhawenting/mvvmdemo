package com.example.user.mvvmdemo.demo5.common.repository.local;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.user.mvvmdemo.demo5.common.bean.Lcee;
import com.example.user.mvvmdemo.demo5.common.bean.User;
import com.example.user.mvvmdemo.demo5.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo5.common.repository.local.service.UserService;
import com.example.user.mvvmdemo.demo5.common.repository.local.service.UserServiceImpl;

/**
 * Created by 86839 on 2017/10/6.
 */

public class LocalUserDataSource implements UserDataSource {
    private static final LocalUserDataSource instance  = new LocalUserDataSource();
    private String TAG = "LocalUserDataSource";

    private LocalUserDataSource(){}
    public static LocalUserDataSource getInstance(){
        return instance;
    }

    private UserService userService = UserServiceImpl.getInstance();

    @Override
    public void queryUserByUserName(final String username, final Result<User> result) {
        new AsyncTask<Void, Void, Object>() {
            @Override
            protected Object doInBackground(Void... voids) {
                try {
                    User user = userService.queryByUsername(username);
                    return user;
                } catch (Exception e) {
                    e.printStackTrace();
                    return e;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                if(o instanceof User)
                    result.onSuccess((User) o);
                else if(o instanceof Exception)
                    result.onFailed((Throwable) o);
                else
                    result.onFailed(null);
            }
        }.execute();

    }

    public void addUser(final User user){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Log.e(TAG, "doInBackground: addUser" );
                userService.add(user);
                return null;
            }

        }.execute();
    }
}
