package com.example.user.mvvmdemo.demo5.common.repository.local.service;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo5.common.bean.User;


/**
 * Created by 86839 on 2017/10/7.
 */

public interface UserService {
    Long add(User user);

    User queryByUsername(String username) throws Exception;
}
