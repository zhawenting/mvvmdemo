package com.example.user.mvvmdemo.demo5.common.repository.local.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.example.user.mvvmdemo.demo5.common.bean.User;
import com.example.user.mvvmdemo.demo5.common.repository.local.dao.UserDao;
import com.example.user.mvvmdemo.demo5.common.repository.local.db.DBHelper;

/**
 * Created by 86839 on 2017/10/7.
 */

public class UserServiceImpl implements UserService {
    public String TAG  ="UserServiceImpl";

    private static final UserServiceImpl instance = new UserServiceImpl();

    private UserServiceImpl() {
    }

    public static UserServiceImpl getInstance() {
        return instance;
    }

    private UserDao userDao  = DBHelper.getInstance().getDb().getUserDao();


    @Override
    public Long add(final User user) {
        return userDao.add(user);
    }

    @Override
    public User queryByUsername(String username) throws Exception {
        return userDao.queryByUsername(username);
    }


}
