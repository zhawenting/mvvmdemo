package com.example.user.mvvmdemo.demo5.common.repository.remote;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.example.user.mvvmdemo.demo5.common.bean.Lcee;
import com.example.user.mvvmdemo.demo5.common.bean.User;
import com.example.user.mvvmdemo.demo5.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo5.common.repository.local.LocalUserDataSource;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 86839 on 2017/10/6.
 */

public class RemoteUserDataSource implements UserDataSource {
    private static final RemoteUserDataSource instance = new RemoteUserDataSource();
    private String TAG ="RemoteUserDataSource";

    private RemoteUserDataSource() {
    }

    public static RemoteUserDataSource getInstance() {
        return instance;
    }


    private UserApi userApi = RetrofitFactory.getInstance().create(UserApi.class);

    @Override
    public void queryUserByUserName(String username, final Result<User> result) {
        userApi.queryUserByUsername(username)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        User user = response.body();
                        result.onSuccess(user);
                        LocalUserDataSource.getInstance().addUser(user);
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.e(TAG, "onFailure: "+t.getMessage() );
                        result.onFailed(t);
                    }
                });
    }
}
