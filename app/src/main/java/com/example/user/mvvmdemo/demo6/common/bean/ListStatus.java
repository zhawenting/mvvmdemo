package com.example.user.mvvmdemo.demo6.common.bean;

public enum ListStatus {
    Refreshing,
    LoadingMore,
    Content,
}
