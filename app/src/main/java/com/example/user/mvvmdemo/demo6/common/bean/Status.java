package com.example.user.mvvmdemo.demo6.common.bean;

public enum Status {

    Loading,
    Content,
    Empty,
    Error,
}
