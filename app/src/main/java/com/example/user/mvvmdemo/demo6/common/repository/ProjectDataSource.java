package com.example.user.mvvmdemo.demo6.common.repository;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo6.common.bean.Lcee;
import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;

public interface ProjectDataSource {
    LiveData<Lcee<Projects>> queryProjects(int page);
}
