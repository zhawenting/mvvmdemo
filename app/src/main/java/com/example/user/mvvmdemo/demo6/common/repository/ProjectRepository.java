package com.example.user.mvvmdemo.demo6.common.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.example.user.mvvmdemo.demo6.common.bean.Lcee;
import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;
import com.example.user.mvvmdemo.demo6.common.repository.local.LocalProjectDataSource;
import com.example.user.mvvmdemo.demo6.common.repository.remote.RemoteProjectDataSource;
import com.example.user.mvvmdemo.demo6.common.utils.NetworkUtils;

public class ProjectRepository {

    private static final ProjectRepository instance = new ProjectRepository();
    private ProjectRepository() {
    }
    public static ProjectRepository getInstance() {
        return instance;
    }


    private Context context;
    private ProjectDataSource remoteProjectDataSource = RemoteProjectDataSource.getInstance();
    private ProjectDataSource localProjectDataSource = LocalProjectDataSource.getInstance();

    public void init(Context context) {
        this.context = context.getApplicationContext();
    }

    public LiveData<Lcee<Projects>> getProjects(int page) {
        if (NetworkUtils.isConnected(context)) {
            return remoteProjectDataSource.queryProjects(page);
        } else {
            return localProjectDataSource.queryProjects(page);
        }
    }


}
