package com.example.user.mvvmdemo.demo6.common.repository.local;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.example.user.mvvmdemo.demo6.common.bean.Lcee;
import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;
import com.example.user.mvvmdemo.demo6.common.repository.ProjectDataSource;
import com.example.user.mvvmdemo.demo6.common.repository.local.service.ProjectService;
import com.example.user.mvvmdemo.demo6.common.repository.local.service.ProjectServiceImpl;

public class LocalProjectDataSource implements ProjectDataSource{
    private static final LocalProjectDataSource instance = new LocalProjectDataSource();

    private LocalProjectDataSource() {
    }

    public static LocalProjectDataSource getInstance() {
        return instance;
    }


    private ProjectService projectsService = ProjectServiceImpl.getInstance();

    public LiveData<Long> addProjects(Projects projects){
        projects.itemsToJson();
        return projectsService.add(projects);
    }

    @Override
    public LiveData<Lcee<Projects>> queryProjects(int page) {
        final MediatorLiveData<Lcee<Projects>> data = new MediatorLiveData<>();
        data.setValue(Lcee.<Projects>loading());
        data.addSource(projectsService.queryProjects(page), new Observer<Projects>() {
            @Override
            public void onChanged(@Nullable Projects projects) {
                if(null==projects)
                    data.setValue(Lcee.<Projects>empty());
                else{
                    projects.getItemsJson();
                    data.setValue(Lcee.content(projects));
                }
            }
        });
        return data;
    }
}
