package com.example.user.mvvmdemo.demo6.common.repository.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;

@Dao
public interface ProjectDao {
    @Insert(onConflict= OnConflictStrategy.REPLACE)
    Long add(Projects projects);

    @Query("select * from projects where page=:page")
    LiveData<Projects> queryProjects(int page);
}
