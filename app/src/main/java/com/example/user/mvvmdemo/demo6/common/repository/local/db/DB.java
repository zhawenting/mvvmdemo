package com.example.user.mvvmdemo.demo6.common.repository.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;
import com.example.user.mvvmdemo.demo6.common.repository.local.dao.ProjectDao;

/**
 * Created by 86839 on 2017/10/7.
 */
@Database(entities = {Projects.class}, version = 1, exportSchema = false)
public abstract class DB extends RoomDatabase {
    public abstract ProjectDao getProjectsDao();
}
