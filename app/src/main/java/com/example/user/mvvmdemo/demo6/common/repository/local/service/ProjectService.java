package com.example.user.mvvmdemo.demo6.common.repository.local.service;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;

public interface ProjectService {

    LiveData<Long> add(Projects projects);

    LiveData<Projects> queryProjects(int page);
}
