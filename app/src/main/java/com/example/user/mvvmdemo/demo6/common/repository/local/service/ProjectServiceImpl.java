package com.example.user.mvvmdemo.demo6.common.repository.local.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;
import com.example.user.mvvmdemo.demo6.common.repository.local.dao.ProjectDao;
import com.example.user.mvvmdemo.demo6.common.repository.local.db.DBHelper;

public class ProjectServiceImpl implements ProjectService{

    private static final ProjectServiceImpl instance = new ProjectServiceImpl();

    private ProjectServiceImpl(){}

    public static ProjectServiceImpl getInstance() {
        return instance;
    }

    private ProjectDao projectDao = DBHelper.getInstance().getDb().getProjectsDao();


    @Override
    public LiveData<Long> add(final Projects projects) {

        final MutableLiveData<Long> data = new MutableLiveData<>();
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                return projectDao.add(projects);
            }

            @Override
            protected void onPostExecute(Long rowId) {
                data.setValue(rowId);
            }
        };

        return data;
    }

    @Override
    public LiveData<Projects> queryProjects(int page) {
        return projectDao.queryProjects(page);
    }
}
