package com.example.user.mvvmdemo.demo6.features.projects;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.user.mvvmdemo.R;
import com.example.user.mvvmdemo.demo6.common.repository.local.db.DB;
import com.example.user.mvvmdemo.demo6.common.repository.local.db.DBHelper;
import com.example.user.mvvmdemo.demo6.common.bean.Lcee;
import com.example.user.mvvmdemo.demo6.common.bean.ListStatus;
import com.example.user.mvvmdemo.demo6.common.bean.Status;
import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;
import com.example.user.mvvmdemo.demo6.common.repository.ProjectRepository;

public class Demo6ReadMoreActivity extends AppCompatActivity {

    private static final String TAG = Demo6ReadMoreActivity.class.getName();
    // vm
    private ProjectViewModel projectsViewModel;

    // view
    private View vContent;
    private View vError;
    private View vLoading;
    private View vEmpty;

    private SwipeRefreshLayout srl;
    private RecyclerView rv;
    private ProjectAdapter projectsAdapter;

    private Status status;
    private ListStatus listStatus = ListStatus.Content;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo6_read_more);
        initView();
        initData();
        initEvent();

    }

    private void initEvent() {
        View.OnClickListener reloadClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reload();
            }
        };
        vError.setOnClickListener(reloadClickListener);
        vEmpty.setOnClickListener(reloadClickListener);

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isLoading()) {
                    srl.setRefreshing(false);
                    return;
                }
            }
        });

        rv.setOnScrollListener(new RecyclerView.OnScrollListener() {
            public int mLastVisibleItemPosition;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager)
                    mLastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                if (projectsAdapter == null)
                    return;
                if (newState == RecyclerView.SCROLL_STATE_IDLE &&
                        mLastVisibleItemPosition + 1 == projectsAdapter.getItemCount()) {
                    if (isLoading())
                        return;
                    listStatus = ListStatus.LoadingMore;
                    loadMore();
                }
            }


        });
    }
    private void loadMore() {
        projectsViewModel.loadMore(projectsAdapter.getItemCount());
    }
    private void initData() {
        DBHelper.getInstance().init(this);
        ProjectRepository.getInstance().init(this);
        projectsViewModel = ViewModelProviders.of(this).get(ProjectViewModel.class);
        projectsViewModel.getIdProjects().observe(this, new Observer<Lcee<Projects>>() {
            @Override
            public void onChanged(@Nullable Lcee<Projects> projectsLcee) {

                updateView(projectsLcee);
            }
        });
        reload();
    }

    private void reload() {
        projectsViewModel.reload();
    }

    private void updateView(Lcee<Projects> projectsLcee) {
        switch (projectsLcee.status) {
            case Content:
                updateContentView(projectsLcee.data);
                break;
            case Error:
                updateErrorView();
                break;
            case Empty:
                updateEmptyView();
                break;
            case Loading:
                updateLoadingView();
                break;
        }
    }

    private void updateLoadingView() {
        switch (listStatus) {
            case LoadingMore: {
                // todo show loading more view in list footer

                break;
            }
            case Refreshing: {

                break;
            }
            default: {
                showLoading();
                break;
            }
        }
    }

    private void updateEmptyView() {
        switch (listStatus) {
            case LoadingMore: {

                break;
            }
            case Refreshing: {
                srl.setRefreshing(false);
                showEmpty();
                break;
            }
            default: {
                showEmpty();
                break;
            }
        }
    }

    private void updateErrorView() {
        switch (listStatus) {
            case Refreshing:
                srl.setRefreshing(false);
                Toast.makeText(this, "Refresh failed", Toast.LENGTH_SHORT).show();
                break;
            case LoadingMore:
                break;
            default:
                showError();
                break;
        }
    }

    private void updateContentView(Projects data) {
        switch (listStatus) {
            case LoadingMore:
                projectsAdapter.addData(data.getItems());
                break;
            case Refreshing:
                srl.setRefreshing(false);
                break;
            default:
                showContent();
                projectsAdapter.setData(data.getItems());
                break;
        }
    }

    private void initView() {
        vContent = findViewById(R.id.v_content);
        vError = findViewById(R.id.v_error);
        vLoading = findViewById(R.id.v_loading);
        vEmpty = findViewById(R.id.v_empty);

        srl = (SwipeRefreshLayout) findViewById(R.id.srl);
        rv = (RecyclerView) findViewById(R.id.rv);

        rv.setLayoutManager(new LinearLayoutManager(this));
        projectsAdapter = new ProjectAdapter();
        rv.setAdapter(projectsAdapter);
    }

    private void showContent() {
        vContent.setVisibility(View.VISIBLE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
        vLoading.setVisibility(View.GONE);
    }

    private void showEmpty() {
        vContent.setVisibility(View.GONE);
        vEmpty.setVisibility(View.VISIBLE);
        vError.setVisibility(View.GONE);
        vLoading.setVisibility(View.GONE);
    }

    private void showError() {
        vContent.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.VISIBLE);
        vLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        vContent.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
        vLoading.setVisibility(View.VISIBLE);
    }

    private boolean isLoading() {
        return status == Status.Loading;
    }


}
