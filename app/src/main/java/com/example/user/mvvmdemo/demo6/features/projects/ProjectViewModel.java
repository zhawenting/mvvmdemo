package com.example.user.mvvmdemo.demo6.features.projects;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.example.user.mvvmdemo.demo6.common.bean.Lcee;
import com.example.user.mvvmdemo.demo6.common.bean.project.Projects;
import com.example.user.mvvmdemo.demo6.common.repository.ProjectRepository;
import com.example.user.mvvmdemo.demo6.common.repository.local.service.ProjectService;
import com.example.user.mvvmdemo.demo6.common.utils.PageUtils;

public class ProjectViewModel extends ViewModel {
    private ProjectRepository projectRepository = ProjectRepository.getInstance();
    private MutableLiveData<Integer> idPage;
    private LiveData<Lcee<Projects>> idProjects;

    public LiveData<Lcee<Projects>> getIdProjects() {
        if(null==idProjects){
            idPage = new MutableLiveData<>();
            idProjects = Transformations.switchMap(idPage, new Function<Integer, LiveData<Lcee<Projects>>>() {
                @Override
                public LiveData<Lcee<Projects>> apply(Integer page) {
                    return projectRepository.getProjects(page);
                }
            });
        }

        return idProjects;
    }

    public void reload(){
        idPage.setValue(1);
    }
    public void loadMore(int currentCount){
        idPage.setValue(PageUtils.getPage(currentCount));
    }
}
