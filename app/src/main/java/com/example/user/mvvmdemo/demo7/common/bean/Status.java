package com.example.user.mvvmdemo.demo7.common.bean;

public enum Status {

    Loading,
    Content,
    Empty,
    Error,
}
