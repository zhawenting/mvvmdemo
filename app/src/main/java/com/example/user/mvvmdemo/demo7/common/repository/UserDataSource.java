package com.example.user.mvvmdemo.demo7.common.repository;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo7.common.bean.Lcee;
import com.example.user.mvvmdemo.demo7.common.bean.User;

public interface UserDataSource {
    LiveData<Lcee<User>> queryUserByUserName(String username);
}
