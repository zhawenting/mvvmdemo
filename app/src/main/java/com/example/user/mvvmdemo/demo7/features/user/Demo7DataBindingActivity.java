package com.example.user.mvvmdemo.demo7.features.user;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.user.mvvmdemo.R;

public class Demo7DataBindingActivity extends AppCompatActivity {

    private static final String TAG = Demo7DataBindingActivity.class.getName();

    private UserViewModel userViewModel;
    private ViewDataBinding viewDataBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
        initEvent();



    }

    private void initEvent() {

    }

    private void initData() {
    }

    private void initView() {

    }
}
