package com.example.user.mvvmdemo.demo8.common.bean;

public enum Status {

    Loading,
    Content,
    Empty,
    Error,
}
