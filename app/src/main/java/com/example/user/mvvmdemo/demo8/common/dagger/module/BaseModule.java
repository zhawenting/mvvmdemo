package com.example.user.mvvmdemo.demo8.common.dagger.module;

import android.content.Context;

import com.example.user.mvvmdemo.BaseApplication;

import dagger.Provides;

public class BaseModule {
//    @Provides
    Context provideContext(){
        return BaseApplication.getAppContext();
    }
}
