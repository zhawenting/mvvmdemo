package com.example.user.mvvmdemo.demo8.common.dagger.module;

import android.content.Context;
import android.widget.RemoteViews;

import com.example.user.mvvmdemo.demo8.common.repository.local.dao.UserDao;
import com.example.user.mvvmdemo.demo8.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo8.common.repository.UserRepository;
import com.example.user.mvvmdemo.demo8.common.repository.local.service.UserService;
import com.example.user.mvvmdemo.demo8.common.repository.local.service.UserServiceImpl;
import com.example.user.mvvmdemo.demo8.common.repository.remote.RemoteUserDataSource;
import com.example.user.mvvmdemo.demo8.common.dagger.qualifier.Local;
import com.example.user.mvvmdemo.demo8.common.dagger.qualifier.Remote;
import com.example.user.mvvmdemo.demo8.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo8.common.repository.remote.RetrofitFactory;
import com.example.user.mvvmdemo.demo8.common.repository.remote.UserApi;

import javax.inject.Singleton;

import dagger.Provides;

public class UserViewModelModule {

//    @Singleton
//    @Provides
//    UserRepository provideUserRepository(Context context, @Remote UserDataSource remoteUserDataSource,
//                                         @Local UserDataSource localUserDataSource) {
//        return new UserRepository(context, remoteUserDataSource, localUserDataSource);
//    }
//
//    @Remote
//    @Singleton
//    @Provides
//    UserDataSource provideRemoteUserDataSource(UserApi userApi,LocalUserDataSource localUserDataSource){
//        return new RemoteUserDataSource(userApi,localUserDataSource);
//    }
//
//    @Singleton
//    @Provides
//    UserApi provideUserApi(){
//        return RetrofitFactory.getInstance().create(UserApi.class);
//    }
//
//    @Local
//    @Singleton
//    @Provides
//    UserDataSource provideLocalUserDataSource(UserService userService){
//        return new LocalUserDataSource(userService);
//    }
//
//    @Singleton
//    @Provides
//    UserService provideUserService(UserDao userDao){
//        return new UserServiceImpl(userDao);
//    }
//
//
}
