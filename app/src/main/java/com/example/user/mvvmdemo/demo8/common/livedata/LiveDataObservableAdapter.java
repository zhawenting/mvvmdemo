package com.example.user.mvvmdemo.demo8.common.livedata;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo8.common.bean.Lcee;

import io.reactivex.Observable;

public class LiveDataObservableAdapter {
    public static <T> LiveData<T> fromObservable(final Observable<T> observable){
        return new ObservableLiveData<>(observable);

    }

    public static <T> LiveData<Lcee<T>> fromObservableLcee(final Observable<T> observable){
        return new ObservableLceeLiveData<>(observable);
    }
}
