package com.example.user.mvvmdemo.demo8.common.livedata;

import android.arch.lifecycle.LiveData;

import com.example.user.mvvmdemo.demo8.common.bean.Lcee;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class ObservableLceeLiveData<T> extends LiveData<Lcee<T>>{
    private WeakReference<Disposable> mDisposableRef;
    private final Observable<T> mObservable;
    private final Object mLock = new Object();

    ObservableLceeLiveData(@NonNull final Observable<T> observable){
        mObservable = observable;
    }

    @Override
    protected void onActive() {
        super.onActive();
        mObservable.subscribe(new Observer<T>() {
            @Override
            public void onSubscribe(Disposable d) {
                synchronized (mLock) {
                    mDisposableRef = new WeakReference<>(d);
                }
            }

            @Override
            public void onNext(T t) {
                if(null==t)
                    postValue(Lcee.<T>empty());
                else
                    postValue(Lcee.content(t));
            }

            @Override
            public void onError(Throwable e) {
                synchronized (mLock) {
                    mDisposableRef = null;
                }
            }

            @Override
            public void onComplete() {
                synchronized (mLock){
                    mDisposableRef = null;
                }
            }
        });
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        synchronized (mLock){
            WeakReference<Disposable> subscriptionRef = mDisposableRef;
            if(subscriptionRef!=null){
                Disposable subscription = subscriptionRef.get();
                if(subscription!=null)
                    subscription.dispose();
                mDisposableRef =null;
            }

        }
    }
}
