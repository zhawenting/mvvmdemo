package com.example.user.mvvmdemo.demo8.common.livedata;

import android.arch.lifecycle.LiveData;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class ObservableLiveData<T> extends LiveData<T> {
    private WeakReference<Disposable> mDisposableRef;
    private final Observable<T> mObservable;
    private final Object mLock = new Object();

    ObservableLiveData(@NonNull final Observable<T> observable){
        mObservable = observable;
    }

    @Override
    protected void onActive() {
        super.onActive();

        mObservable.subscribe(new Observer<T>() {
            @Override
            public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                synchronized (mLock){
                    mDisposableRef = new WeakReference<>(d);
                }
            }

            @Override
            public void onNext(@io.reactivex.annotations.NonNull T t) {
                postValue(t);
            }

            @Override
            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                synchronized (mLock){
                    mDisposableRef = null;
                }
                throw new RuntimeException(e);
            }

            @Override
            public void onComplete() {
                synchronized (mLock) {
                    mDisposableRef = null;
                }
            }
        });
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        synchronized (mLock){
            WeakReference<Disposable> subscriptionRef = mDisposableRef;
            if(subscriptionRef!=null){
                Disposable subscrption =subscriptionRef.get();
                if(subscrption!=null)
                    subscrption.dispose();
                mDisposableRef = null;
            }
        }
    }
}
