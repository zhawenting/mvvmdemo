package com.example.user.mvvmdemo.demo8.common.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.example.user.mvvmdemo.demo8.common.bean.Lcee;
import com.example.user.mvvmdemo.demo8.common.bean.User;
import com.example.user.mvvmdemo.demo8.common.dagger.qualifier.Local;
import com.example.user.mvvmdemo.demo8.common.dagger.qualifier.Remote;
import com.example.user.mvvmdemo.demo8.common.repository.local.LocalUserDataSource;
import com.example.user.mvvmdemo.demo8.common.repository.remote.RemoteUserDataSource;
import com.example.user.mvvmdemo.demo8.common.utils.NetworkUtils;

import javax.inject.Inject;

public class UserRepository {


    private UserDataSource localUserDataSource;
    private UserDataSource remoteUserDataSource;

    private Context context;

    @Inject
    public UserRepository(Context context, @Remote UserDataSource remoteUserDataSource,
                           @Local UserDataSource localUserDataSource){
        this.context = context;
        this.remoteUserDataSource = remoteUserDataSource;
        this.localUserDataSource = localUserDataSource;
    };

    public LiveData<Lcee<User>> getUser(String username) {
        if (NetworkUtils.isConnected(context)) {
            return remoteUserDataSource.queryUserByUserName(username);
        } else {
            return localUserDataSource.queryUserByUserName(username);
        }
    }


}
