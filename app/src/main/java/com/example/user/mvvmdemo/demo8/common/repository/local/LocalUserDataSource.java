package com.example.user.mvvmdemo.demo8.common.repository.local;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.example.user.mvvmdemo.demo8.common.bean.Lcee;
import com.example.user.mvvmdemo.demo8.common.bean.User;
import com.example.user.mvvmdemo.demo8.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo8.common.repository.local.service.UserService;
import com.example.user.mvvmdemo.demo8.common.repository.local.service.UserServiceImpl;

/**
 * Created by 86839 on 2017/10/6.
 */

public class LocalUserDataSource implements UserDataSource {

    public LocalUserDataSource(UserService userService){
        this.userService = userService;
    }


    private UserService userService;



    public LiveData<Long> addUser(final User user){
        return userService.add(user);
    }

    @Override
    public LiveData<Lcee<User>> queryUserByUserName(String username) {
        final MediatorLiveData<Lcee<User>> data = new MediatorLiveData<>();
        data.setValue(Lcee.<User>loading());
        data.addSource(userService.queryByUsername(username), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
               if(null==data)
                   data.setValue(Lcee.<User>empty());
               else
                   data.setValue(Lcee.content(user));
            }
        });
        return data;
    }
}
