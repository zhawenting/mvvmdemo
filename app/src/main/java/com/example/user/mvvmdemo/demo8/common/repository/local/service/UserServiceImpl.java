package com.example.user.mvvmdemo.demo8.common.repository.local.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.example.user.mvvmdemo.demo8.common.bean.User;
import com.example.user.mvvmdemo.demo8.common.repository.local.dao.UserDao;
import com.example.user.mvvmdemo.demo8.common.repository.local.db.DBHelper;

/**
 * Created by 86839 on 2017/10/7.
 */

public class UserServiceImpl implements UserService {
    public String TAG = "UserServiceImpl";


    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    private UserDao userDao;


    @Override
    public LiveData<Long> add(final User user) {
        final MutableLiveData<Long> data = new MutableLiveData<>();
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                return userDao.add(user);
            }

            @Override
            protected void onPostExecute(Long rowId) {
                data.setValue(rowId);
            }
        }.execute();

        return data;
    }

    @Override
    public LiveData<User> queryByUsername(String username){
        return userDao.queryByUsername(username);
    }


}
