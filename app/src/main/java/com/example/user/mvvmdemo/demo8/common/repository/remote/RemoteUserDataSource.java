package com.example.user.mvvmdemo.demo8.common.repository.remote;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.user.mvvmdemo.demo8.common.bean.Lcee;
import com.example.user.mvvmdemo.demo8.common.bean.User;
import com.example.user.mvvmdemo.demo8.common.repository.UserDataSource;
import com.example.user.mvvmdemo.demo8.common.repository.local.LocalUserDataSource;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 86839 on 2017/10/6.
 */

public class RemoteUserDataSource implements UserDataSource {
    private String TAG = "RemoteUserDataSource";
    LocalUserDataSource localUserDataSource;

    public RemoteUserDataSource(UserApi userApi,LocalUserDataSource localUserDataSource) {
       this.userApi = userApi;
       this.localUserDataSource = localUserDataSource;

    }

    private UserApi userApi;


    @Override
    public LiveData<Lcee<User>> queryUserByUserName(String username) {
        final MutableLiveData<Lcee<User>> data = new MutableLiveData<>();
        data.setValue(Lcee.<User>loading());
        userApi.queryUserByUsername(username)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        User user = response.body();
                        if(null==user) {
                            data.setValue(Lcee.<User>empty());
                            return;
                        } else
                            data.setValue(Lcee.content(user));
                        localUserDataSource.addUser(user);
                    }


                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        data.setValue(Lcee.<User>error(t));
                    }
                });
        return data;
    }
}
