package com.example.user.mvvmdemo.demo8.feature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.user.mvvmdemo.R;

public class UserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo8_user);
    }
}
